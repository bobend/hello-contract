﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelloContract.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using NJsonSchema;
using NJsonSchema.Generation;
using NJsonSchema.Generation.TypeMappers;

namespace HelloContract.Controllers
{
    [Route("api/[controller]")]
    public class ContractController : Controller
    {
        [HttpGet]
        public async Task<string> Get()
        {
            
            var schema = await JsonSchema4.FromTypeAsync<FooMessage>(new JsonSchemaGeneratorSettings
            {
                DefaultEnumHandling = EnumHandling.String,
                //GenerateAbstractProperties = true,
                GenerateXmlObjects = true,
                //NullHandling = NullHandling.Swagger,
                //DefaultPropertyNameHandling = PropertyNameHandling.CamelCase,
                
              


            });


            var test = schema.ToJson();
            return test;
        }
        
    }
}
