﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace HelloContract.Models
{
    /// <summary>
    /// A FooMessage used as interchange format for Foo Systems
    /// </summary>
    
    public class FooMessage
    {
        /// <summary>
        ///  Unique identifer
        ///  </summary>
        [Required]
        public string Urn { get; set; }

        /// <summary>
        /// Timestamp
        /// </summary>
        [Required]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        [Required]
        public Body Payload { get; set; }

        /// <summary>
        /// Hey, touch my foldy flaps
        /// Grab my terryfolds
        /// Grab my foldy holds
        /// Grab my terry flaps
        /// In my terryfolds
        /// Grab my terry flaps
        /// You gotta touch ’em… my terryfolds
        /// </summary>
        [Required]
        public Folds Terryfold { get; set; }


    }

    /// <summary>
    /// Sexy
    /// </summary>
    public class Body
    {
        /// <summary>
        /// drinking place
        /// </summary>
        [Required]
        public string Bar { get; set; }
        /// <summary>
        /// list of poop ids
        /// </summary>
        public IList<Guid> Poops { get; set; }
    }

    /// <summary>
    /// Major fold types
    /// </summary>
    public enum Folds
    {
        Terry,
        Flap,
        Dance,
        Holds
    }
}
