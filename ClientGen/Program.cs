﻿using System;
using System.IO;
using System.Net.Http;
using NJsonSchema;
using NJsonSchema.CodeGeneration;
using NJsonSchema.CodeGeneration.CSharp;

namespace HelloContract.ClientGen
{
    class Program
    {
        private const string Url = "http://localhost:8971/api/contract";

        static void Main(string[] args)
        {
            Console.WriteLine($"Press any key to get client data from {Url}");
            Console.ReadKey();

            var schema = JsonSchema4.FromUrlAsync(Url).Result;

            var generator = new CSharpGenerator(schema, new CSharpGeneratorSettings()
            {
                Namespace = "HelloContract.Client",
                ClassStyle = CSharpClassStyle.Poco

            });

            var content = generator.GenerateFile();
            using (var file = File.Create("..\\Client\\Client.cs"))
            {
                using (var sw = new StreamWriter(file))
                {
                    sw.Write(content);
                }
            }
            Console.WriteLine(content);

            Console.ReadKey();
        }
    }
}
